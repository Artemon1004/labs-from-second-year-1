

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;

// ������ �������
template <class ValType>
class TVector
{
protected:
	ValType *pVector;
	int Size;       // ������ �������
	int StartIndex; // ������ ������� �������� �������
public:
	TVector(int s = 10, int si = 0);
	TVector(const TVector &v);                // ����������� �����������
	~TVector();
	int GetSize() { return Size; } // ������ �������
	int GetStartIndex() { return StartIndex; } // ������ ������� ��������
	ValType& operator[](int pos);             // ������
	bool operator==(const TVector &v) const;  // ���������
	bool operator!=(const TVector &v) const;  // ���������
	TVector& operator=(const TVector &v);     // ������������


											  // ��������� ��������
	TVector  operator+(const ValType &val);   // ��������� ������
	TVector  operator-(const ValType &val);   // ������� ������
	TVector  operator*(const ValType &val);   // �������� �� ������

											  // ��������� ��������
	TVector  operator+(const TVector &v);     // ��������
	TVector  operator-(const TVector &v);     // ���������
	ValType  operator*(const TVector &v);     // ��������� ������������

											  // ����-�����
	friend istream& operator>>(istream &in, TVector &v)
	{
		for (int i = 0; i < v.Size; i++)
			in >> v.pVector[i];
		return in;
	}
	friend ostream& operator<<(ostream &out, const TVector &v)
	{
		for (int i = 0; i < v.Size; i++)
			out << v.pVector[i] << ' ';
		return out;
	}
};

template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	try
	{
		if (s > MAX_VECTOR_SIZE) throw 1;
		if (si <0) throw 2;
		else
		{
			Size = s;
			StartIndex = si;
			pVector = new ValType[s];
			for (int i = 0; i < Size; i++)
				pVector[i] = 0;
		}
	}
	catch (int error)
	{
		if (error == 1)
			cout << "Error!Size can not be more than 100000000 " << endl;
		if (error == 2)
			cout << "Error!Size can not be less 0 " << endl;
		throw 1;
	}


} /*-------------------------------------------------------------------------*/

template <class ValType> //����������� �����������
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = new ValType[Size];
	for (unsigned int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} /*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������
ValType& TVector<ValType>::operator[](int pos)
{
	try
	{
		if (pos < 0) throw 1;
		if (pos >= Size) throw 2;
		else
			return pVector[pos];
	}
	catch (int error)
	{
		if (error == 1)
			cout << "Error! Position can't be less 0" << endl;
		if (error == 2)
			cout << "Error!Position can be more size" << endl;
		throw 1;

	}

} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator==(const TVector &v) const
{
	if (this == &v)
		return true;
	else
	{
		if ((Size != v.Size) || (StartIndex != v.StartIndex))
			return false;
		else
			for (unsigned int i = StartIndex; i < Size; i++)
			{
				if (pVector[i] != v.pVector[i])
					return false;
			}
		return true;
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator!=(const TVector &v) const
{
	if (this != &v)
		return false;
	else
	{
		if ((Size == v.Size) || (StartIndex == v.StartIndex))
			return false;
		else
			for (unsigned int i = StartIndex; i < Size; i++)
			{
				if (pVector[i] == v.pVector[i])
					return false;
			}
		return true;
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������������
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (this != &v)
	{
		if (v.Size != 0)
		{
			delete[] pVector;
			pVector = new ValType[v.Size];
			for (unsigned int i = 0; i < v.Size; i++)
				pVector[i] = v.pVector[i];
		}
		else
		{
			delete[] pVector;
			Size = 1;
			pVector = new ValType[Size];
			for (unsigned int i = 0; i < Size; i++)
				pVector[i] = 0;
		}
	}

	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	try
	{
		if (Size == 1)
		{
			TVector<ValType> Result(1, 0);
			Result.pVector[0] += val;
			return Result;
		}
		else
			throw 1;

	}
	catch (int error)
	{
		if (error == 1)
		{
			cout << "Error!Size of vector must be equal 1" << endl;
			throw 1;
		}

	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������� ������
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	try
	{
		if (Size == 1)
		{
			TVector<ValType> Result(1, 0);
			Result.pVector[0] -= val;
			return Result;
		}
		else
			throw 1;

	}
	catch (int error)
	{
		if (error == 1)
		{
			cout << "Error!Size of vector must be equal 1" << endl;
			throw 1;
		}

	}

} /*-------------------------------------------------------------------------*/

template <class ValType> // �������� �� ������
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector Result(Size, StartIndex);
	for (unsigned int i = 0; i < Size; i++)
		Result.pVector[i] *= val;
	return Result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size == v.Size)
	{
		TVector Result(Size, StartIndex);
		for (unsigned int i = 0; i < Size; i++)
			Result.pVector[i] = v.pVector[i] + Result.pVector[i];
		return Result;
	}
	else return 0;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size == v.Size)
	{
		TVector Result(Size, StartIndex);
		for (unsigned int i = 0; i < Size; i++)
			Result.pVector[i] = v.pVector[i] - Result.pVector[i];
		return Result;
	}
	else return 0;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������������
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	try
	{
		if (Size != v.Size) throw 1;
		else
			if (StartIndex != v.StartIndex) throw 2;
			else
			{
				ValType result;
				for (unsigned int i = 0; i < Size; i++)
					result += pVector[i] * v.pVector[i];
			}

	}
	catch (int error)
	{
		if (error == 1)
			cout << "Error!Different matrix sizes " << endl;
		throw 1;
		if (error == 2)
			cout << "Error! Different starting vector index" << endl;
		throw 2;
	}
} /*-------------------------------------------------------------------------*/

//void printMenu()
//{
//cout << "������� ������� �����, �������� �������� ��������" << endl
//	<< "1 ��������� ������ � �������" << endl
//	<< "2 ������� ������ �� ������� " << endl
////	<< "3 ��������� ������� �� ������" << endl
//<< "4 �������� ��������" << endl
			//	<< "5 �������� ��������" << endl
			//	<< "6 ��������� ������������ ��������" << endl//
			
			//<< "7 //�������� ����� �� ���������� ��������" << endl
			//<< "8 ����� �� ���������" << endl;
			//}
